"use strict";

import {src, dest, parallel, series} from "gulp";
import fs from "fs";
import sass from "gulp-sass";
import clean from "gulp-clean";
import rename from "gulp-rename";
import watch from "gulp-watch";
import minifyCSS from "gulp-minify-css";
import sourcemaps from "gulp-sourcemaps";
import Mode from "gulp-mode";
import uglify from "gulp-uglify";
import babel from "gulp-babel";
import autoprefixer from "gulp-autoprefixer";
import strip from "gulp-strip-comments";
import stripCSSComments from "gulp-strip-css-comments";
import rmEmptyLine from "gulp-remove-empty-lines";
import BrowserSync from "browser-sync";

let mode = new Mode();
let browserSync = BrowserSync.create();

const __PATHS__ = {
    src: {
        index: "./index.html",
        style: "./style.scss",
        app: "./app.js",
        config: "./config.js",
        mixins: "./_mixins.scss",
        colors: "./_colors.scss",
        libs: {
            js: "libs/*.js"
        },
        img: "./images/*",
        core: {
            components: {
                scss: "./core/components/**/*.component.scss",
                js: "./core/components/**/*.component.js",
                temp: "./core/components/**/*.component.html"
            },
            services: {
                js: "./core/services/*.service.js"
            },
            bundles: {
                components: "./core/components/core.components.js",
                services: "./core/services/core.services.js"
            }
        },
        modules: {
            controllers: {
                scss: "./modules/**/controllers/**/*.page.scss",
                js: "./modules/**/controllers/**/*.page.js",
                temp: "./modules/**/controllers/**/*.page.html"
            },
            services: {
                js: "./modules/**/services/**/*.service.js"
            },
            bundles: {
                // TODO продумать, так как при компиляции будут перезаписаны модульные сервисы
                controllers: "./modules/**/controllers/module.controllers.js",
                services: "./modules/**/services/module.services.js",
                modules: "./modules/modules.js"
            },
            js: "./modules/**/*.module.js"
        }
    },
    build: {
        css: "./public/css",
        js: "./public/js",
        img: "./public/images",
        temp: "./public/tmp"
    }
};

const watchCSS = (path) => {
    watch(path, (file) => {
        return src(file.path)
            .pipe(mode.production(sourcemaps.init()))
            .pipe(stripCSSComments())
            .pipe(mode.development(rmEmptyLine()))
            .pipe(sass.sync().on("error", sass.logError))
            .pipe(autoprefixer())
            .pipe(mode.production(minifyCSS()))
            .pipe(mode.production(sourcemaps.write()))
            .pipe(dest(__PATHS__.build.css))
            .pipe(browserSync.reload({stream: true}));
    });
};

const watchJS = (path) => {
    watch(path, (file) => {
        return src(file.path)
            .pipe(mode.production(sourcemaps.init()))
            .pipe(babel({
                presets: ["es2015"],
                plugins: ["syntax-async-functions", "add-module-exports", "transform-decorators-legacy"]
            }))
            .pipe(mode.production(uglify()))
            .pipe(strip())
            .pipe(rename({dirname: ""}))
            .pipe(mode.production(sourcemaps.write()))
            .pipe(dest(__PATHS__.build.js))
            .pipe(browserSync.reload({stream: true}));
    });
};

const watchHTML = (path) => {
    watch(path, (file) => {
        return src(file.path)
            .pipe(rename({dirname: ""}))
            .pipe(dest(__PATHS__.build.temp))
            .pipe(browserSync.reload({stream: true}));
    });
};

const watchColor = () => {
    watch(__PATHS__.src.colors, series(build_css));

};

const watchMixin = () => {
    watch(__PATHS__.src.mixins, series(build_css));
};

export const clear_css = (done) => {
    if (fs.existsSync(__PATHS__.build.css)) {
        return src(__PATHS__.build.css, {read: false})
            .pipe(clean({
                force: true
            }));
    } else {
        done();
    }
};

export const clear_js = (done) => {
    if (fs.existsSync(__PATHS__.build.js)) {
        return src(__PATHS__.build.js, {read: false})
            .pipe(clean({
                force: true
            }));
    } else {
        done();
    }
};

export const clear_tmp = (done) => {
    if (fs.existsSync(__PATHS__.build.temp)) {
        return src(__PATHS__.build.temp, {read: false})
            .pipe(clean({
                force: true
            }));
    } else {
        done();
    }
};

export const clear_img = (done) => {
    if (fs.existsSync(__PATHS__.build.img)) {
        return src(__PATHS__.build.img, {read: false})
            .pipe(clean({
                force: true
            }));
    } else {
        done();
    }
};

export const clear = parallel(clear_css, clear_img, clear_js, clear_tmp);

export const build_css = series(clear_css, () => {
    return src([
        __PATHS__.src.style,
        __PATHS__.src.core.components.scss,
        __PATHS__.src.modules.controllers.scss
    ])
        .pipe(mode.production(sourcemaps.init()))
        .pipe(sass.sync().on("error", sass.logError))
        .pipe(stripCSSComments())
        .pipe(mode.development(rmEmptyLine()))
        .pipe(autoprefixer())
        .pipe(mode.production(minifyCSS()))
        .pipe(mode.production(sourcemaps.write()))
        .pipe(rename({dirname: ""}))
        .pipe(dest(__PATHS__.build.css))
        .pipe(mode.development(browserSync.reload({stream: true})));
});

export const build_js = series(clear_js, () => {
    return src([
        __PATHS__.src.app,
        __PATHS__.src.config,
        __PATHS__.src.libs.js,
        __PATHS__.src.core.bundles.components,
        __PATHS__.src.core.bundles.services,
        __PATHS__.src.core.components.js,
        __PATHS__.src.core.services.js,
        __PATHS__.src.modules.bundles.controllers,
        __PATHS__.src.modules.bundles.services,
        __PATHS__.src.modules.bundles.modules,
        __PATHS__.src.modules.js,
        __PATHS__.src.modules.controllers.js,
        __PATHS__.src.modules.services.js
    ])
        .pipe(mode.production(sourcemaps.init()))
        .pipe(babel({
            presets: ["es2015"],
            plugins: ["syntax-async-functions", "add-module-exports", "transform-decorators-legacy"]
        }))
        .pipe(mode.production(uglify()))
        .pipe(strip())
        .pipe(rename({dirname: ""}))
        .pipe(mode.production(sourcemaps.write()))
        .pipe(dest(__PATHS__.build.js))
        .pipe(mode.development(browserSync.reload({stream: true})));
});

export const build_tmp = series(clear_tmp, () => {
    return src([
        __PATHS__.src.index,
        __PATHS__.src.core.components.temp,
        __PATHS__.src.modules.controllers.temp
    ])
        .pipe(rename({dirname: ""}))
        .pipe(dest(__PATHS__.build.temp))
        .pipe(mode.development(browserSync.reload({stream: true})));
});

export const build_img = series(clear_img, () => {
    return src(__PATHS__.src.img)
        .pipe(dest(__PATHS__.build.img))
        .pipe(mode.development(browserSync.reload({stream: true})));
});

export const build = parallel(build_css, build_js, build_tmp, build_img);

export const browser = () => {
    browserSync.init({
        port: 8080,
        proxy: "localhost:8080",
        notify: false,
        reloadDelay: 1000,
        ui: {
            port: 8081
        }
    });

    watchJS(__PATHS__.src.app);
    watchJS(__PATHS__.src.config);
    watchJS(__PATHS__.src.libs.js);
    watchCSS(__PATHS__.src.style);
    watchHTML(__PATHS__.src.index);
    /** Компоненты */
    watchJS(__PATHS__.src.core.bundles.components);
    watchJS(__PATHS__.src.core.components.js);
    watchCSS(__PATHS__.src.core.components.scss);
    watchHTML(__PATHS__.src.core.components.temp);
    /** Сервисы */
    watchJS(__PATHS__.src.core.bundles.services);
    watchJS(__PATHS__.src.core.services.js);
    /** Модули */
    watchJS(__PATHS__.src.modules.bundles.modules);
    watchJS(__PATHS__.src.modules.js);
    /** Контроллеры в модуле */
    watchJS(__PATHS__.src.modules.bundles.controllers);
    watchJS(__PATHS__.src.modules.controllers.js);
    watchCSS(__PATHS__.src.modules.controllers.scss);
    watchHTML(__PATHS__.src.modules.controllers.temp);
    /** Сервисы модуля */
    watchJS(__PATHS__.src.modules.services.js);
    watchJS(__PATHS__.src.modules.bundles.services);

    watchColor();
    watchMixin();
};

export const develop = series(build, browser);
