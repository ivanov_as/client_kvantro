"use strict";

import {Service} from "js/fr";

/**
 * Обработка событий отображения элемента на экране
 */

@Service()
export class DisplayEvents {

    static oninit() {
        this._super();
    }

    /** Отображаем элемент
     * @param showClass {String} Класс из библиотеки animate.css
     */
    static show(showClass) {
        let _el = this.el.children[0].classList;

        this.set("isShowing", true);

        if (showClass && !_el.contains(showClass)) {
            _el.add(showClass);
        }
    }

    /** Скрываем элемент
     * @param hideClass {String} Класс из библиотеки animate.css
     */
    static hide(hideClass) {
        let _el = this.el.children[0].classList;

        this.set("isShowing", false);

        if (hideClass && !_el.contains(hideClass)) {
            if (!_el.contains("animated")) _el.add("animated");
            _el.add(hideClass);
        }
    }

    /**
     * @param showClass {String} Класс из библиотеки animate.css
     * @param hideClass {String} Класс из библиотеки animate.css
     */
    static toggle(showClass, hideClass) {

        let _el = this.el.children[0] ? this.el.children[0].classList : undefined;

        /** устанавливаем анимацию */
        if (showClass || hideClass && _el) {
            if (!_el.contains("animated")) _el.add("animated");
            _el.remove(showClass);
            _el.remove(hideClass);

            if (this.get("isShowing")) {
                if (hideClass) {
                    _el.add(hideClass);
                    setTimeout(() => {
                        this.set("isShowing", false);
                    }, 700);
                } else {
                    this.set("isShowing", false);
                }
            } else {
                _el.add(showClass);
                this.set("isShowing", true);
            }

        }
    }
}

@Service()
export class DataEvents {
    static refresh() {

    }

    static reload() {

    }
}