"use strict";

import {Service} from "js/fr";

@Service()
export class StorageService {

    static oninit() {
        this._super();
    }

    // Сохранение данных в хранилище
    static setLocalStorageItem(value) {
        window.localStorage.setItem(this._guid, value);
    }

    // Получение данных из хранилища
    static getLocalStorageItem() {
        return window.localStorage.getItem(this._guid);
    }

    static setSessionStorageItem(value) {
        window.sessionStorage.setItem(this._guid, value);
    }

    static getSessionStorageItem() {
        return window.sessionStorage.getItem(this._guid);
    }
}