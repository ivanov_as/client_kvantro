"use strict";

import {Service} from "js/fr";

@Service()
export class HttpService {

    static oninit() {
        this._super();
    }

    /**
     * Получение данных с сервера
     */
    static $get() {
        this.set("isLoad", true);
        let path = this.get("path");

        if (path) {
            $.get(path, this.getRequestParams()).done((res) => {
                setTimeout(() => {
                    this.setDataSet(res.data);
                    console.log(this.getDataSet());
                    this.set("isLoad", false);
                }, 2000);
            }).fail(() => {
                setTimeout(() => {
                    this.set("isLoad", false);
                }, 2000);
            });
        }
    }

    /**
     * Получение и преобразование данных в формат ключ/значение с сервера
     */
    static $getFormatKeyValue() {

        let idFieldName = this.get("keyField");
        let valueFieldName = this.get("valueField");
        let path = this.get("path");

        if (!idFieldName || !valueFieldName)
            throw new Error("Not found id or value field");

        if (path) {
            $.get(path, this.getRequestParams())
                .done((res) => {
                    this.setDataSet(_transform(res.data));
                    // console.log(this.getDataSet());
                }).fail((err) => {

            });
        } else {
            if (this.getDataSet()) {
                this.setDataSet(_transform(this.getDataSet().getRecords()));
            }
        }

        function _transform(data) {
            let lng, _data = [], _row;
            if (typeof data !== "undefined" && Array.isArray(data)) {
                lng = data.length;
                for (let idx = 0; idx < lng; idx++) {
                    _row = {};
                    _row["id"] = data[idx][idFieldName];
                    _row["value"] = data[idx][valueFieldName];
                    _data.push(_row);
                }
            }
            return _data;
        }
    }

    static setRequestParams(params) {
        let _requestParams = {
            offset: this.get("offset") || 0
        };
        if (params) {
            for (let param in params) {
                if (typeof params[param] !== "undefined") {
                    _requestParams[param] = params[param];
                }
            }
        }
        this.set("requestParams", _requestParams);
        return this.getRequestParams();
    }

    static getRequestParams() {
        let _requestParams = this.get("requestParams") || {};
        if (this.get("offset")) _requestParams["offset"] = this.get("offset");
        return _requestParams;
    }

    static rmRequestParams() {
        this.set("requestParams", {});
    }
}