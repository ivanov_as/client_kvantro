"use strict";

import {Component} from "js/fr";
import tmp from "tmp/rating.component.html!text";
import style from "css/rating.component.css!text";

@Component({
    selector: "rating-box",
    template: tmp,
    theme: style,
    services: []
})
export class RatingComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}