"use strict";

import {Component} from "js/fr";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import {NewsComponent} from "js/news.component";
import tmp from "tmp/news-list.component.html!text";
import style from "css/news-list.component.css!text";

@Component({
    selector: "news-list",
    template: tmp,
    theme: style,
    components: [NewsComponent],
    services: [DataService, HttpService]
})
export class NewsListComponent {

    static oninit() {
        this._super();
    }

    static onrender() {

    }

    static oncomplete() {
        this.$get();
    }
}