"use strict";

import {Component} from "js/fr";
import tmp from "tmp/button-message.component.html!text";
import style from "css/button-message.component.css!text";

@Component({
    selector: "btn-msg",
    template: tmp,
    theme: style,
    services: []
})
export class BtnMsgComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}