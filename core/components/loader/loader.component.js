"use strict";

import {Component} from "js/fr";
import tmp from "tmp/loader.component.html!text";
import style from "css/loader.component.css!text";

@Component({
    selector: "loader",
    template: tmp,
    theme: style,
    services: []
})
export class LoaderComponent {

    static oninit() {
        this._super();
    }

    static onrender() {

    }
}