"use strict";

import {Component} from "js/fr";
import {StorageService} from "js/storage.service";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/one.component.html!text";
import style from "css/one.component.css!text";

@Component({
    selector: "one-component",
    template: tmp,
    theme: style,
    services: [DataService, HttpService, StorageService]
})
export class OneComponent {

    static oninit() {
        this._super();
        this.setLocalStorageItem("hello world"); // Test storage
    }

    static onrender() {
    }

    static reservedBtnClick(event) {
    }
}