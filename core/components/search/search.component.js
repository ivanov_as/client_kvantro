"use strict";

import {Component} from "js/fr";
import {LookupComponent} from "js/lookup.component";
import {RangerComponent} from "js/ranger.component";
import {StorageService} from "js/storage.service";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import {DisplayEvents} from "js/events.service";
import tmp from "tmp/search.component.html!text";
import style from "css/search.component.css!text";

@Component({
    selector: "search-panel",
    template: tmp,
    theme: style,
    components: [LookupComponent, RangerComponent],
    services: [
        DataService,
        HttpService,
        StorageService,
        DisplayEvents
    ]
})
export class SearchPanelComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }

    static oncomplete() {
    }
}