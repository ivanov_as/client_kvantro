"use strict";

import {Component} from "js/fr";
import {StorageService} from "js/storage.service";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/news.component.html!text";
import style from "css/news.component.css!text";

@Component({
    selector: "news",
    template: tmp,
    theme: style,
    services: [DataService, HttpService, StorageService]
})
export class NewsComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}