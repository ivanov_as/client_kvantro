"use strict";

import {Component} from "js/fr";
import {StorageService} from "js/storage.service";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/ranger.component.html!text";
import style from "css/ranger.component.css!text";

@Component({
    selector: "ranger",
    template: tmp,
    theme: style,
    services: [DataService, HttpService, StorageService]
})
export class RangerComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
        $(".ios-ranger").ionRangeSlider({
            type: "single",
            min: 0,
            max: 100,
            from: 50,
            onChange: (data) => {
                console.log(data.from);
            }
        });
    }

    static oncomplete() {

    }
}