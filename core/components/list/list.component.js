"use strict";

import {Component} from "js/fr";
import {OneComponent} from "js/one.component";
import {LookupComponent} from "js/lookup.component";
import {LoaderComponent} from "js/loader.component";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/list.component.html!text";
import style from "css/list.component.css!text";

@Component({
    selector: "list-component",
    template: tmp,
    theme: style,
    components: [OneComponent, LookupComponent, LoaderComponent],
    services: [DataService, HttpService]
})
export class ListComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
        this._super();
    }

    static oncomplete() {
        this.refresh();
    }

    static refreshList() {
        this.nextList();
        this.refresh();
    }

    static refresh() {
        this.$get();
    }
}