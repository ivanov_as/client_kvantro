"use strict";

import {Component} from "js/fr";
import {StorageService} from "js/storage.service";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/certificate.component.html!text";
import style from "css/certificate.component.css!text";

@Component({
    selector: "certificate",
    template: tmp,
    theme: style,
    services: [DataService, HttpService, StorageService]
})
export class CertificateComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}