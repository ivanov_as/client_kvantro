"use strict";

import {BtnMsgComponent} from "js/button-message.component";
import {CertificateComponent} from "js/certificate.component";
import {ListComponent} from "js/list.component";
import {LoaderComponent} from "js/loader.component";
import {LookupComponent} from "js/lookup.component";
import {NewsComponent} from "js/news.component";
import {NewsListComponent} from "js/news-list.component";
import {OneComponent} from "js/one.component";
import {QuestsComponent} from "js/quests.component";
import {RangerComponent} from "js/ranger.component";
import {ReviewComponent} from "js/review.component";
import {SearchPanelComponent} from "js/search.component";
import {RatingComponent} from "js/rating.component";
import {QuestsCertificateComponent} from "js/quests-certificate.component";

export {
    BtnMsgComponent,
    CertificateComponent,
    ListComponent,
    LoaderComponent,
    LookupComponent,
    NewsComponent,
    NewsListComponent,
    OneComponent,
    QuestsComponent,
    RangerComponent,
    ReviewComponent,
    SearchPanelComponent,
    RatingComponent,
    QuestsCertificateComponent
}




