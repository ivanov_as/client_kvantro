"use strict";

import {Component} from "js/fr";
import {RatingComponent} from "js/rating.component";
import tmp from "tmp/quests-certificate.component.html!text";
import style from "css/quests-certificate.component.css!text";

@Component({
    selector: "quests-certificate",
    template: tmp,
    theme: style,
    components: [RatingComponent],
    services: []
})
export class QuestsCertificateComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}