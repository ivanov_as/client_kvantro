"use strict";

import {Component} from "js/fr";
import tmp from "tmp/quests.component.html!text";
import style from "css/quests.component.css!text";

@Component({
    selector: "quests-list",
    template: tmp,
    theme: style,
    services: []
})
export class QuestsComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }
}