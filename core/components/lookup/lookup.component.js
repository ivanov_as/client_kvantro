"use strict";

import {Component} from "js/fr";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/lookup.component.html!text";
import style from "css/lookup.component.css!text";

@Component({
    selector: "lookup",
    template: tmp,
    theme: style,
    components: [],
    services: [DataService, HttpService]
})
export class LookupComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
        let _self = this;

        let $select2 = $(`[name="${this.get("name")}"]`);
        $select2.select2({
            dropdownAutoWidth: true,
            width: "100%",
            height: "100%"
        });

        $select2.on("select2:select", (e) => {
            let recordIdx = e.params.data.element.index,
                _record = _self.getDataSet().getRecords(recordIdx);

            _self.getDataSet().setCurrentRecord(_record);
            _self.updateDataSet();
        });

        _self.observeDataSet((newDataSet) => {
            if (newDataSet) {
                $select2.val(newDataSet.getCurrentRecord().id).trigger('change.select2');
                $select2.select2({
                    dropdownAutoWidth: true,
                    width: "100%",
                    height: "100%"
                });
            }
        });
        _self._super();
    }

    static refresh() {
        this.$getFormatKeyValue();
    }

    static oncomplete() {
        this.refresh();
    }
}