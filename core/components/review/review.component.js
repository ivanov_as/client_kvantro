"use strict";

import {Component} from "js/fr";
import {HttpService} from "js/http.service";
import {DataService} from "js/data.service";
import tmp from "tmp/review.component.html!text";
import style from "css/review.component.css!text";

@Component({
    selector: "review",
    template: tmp,
    theme: style,
    components: [],
    services: [DataService, HttpService]
})
export class ReviewComponent {

    static oninit() {
        this._super();
    }

    static onrender() {
    }

    static complete() {
    }

    static refresh(event) {
    }
}