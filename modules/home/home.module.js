"use strict";

import {Module} from "js/fr";
import * as controllers from "js/module.controllers";
import * as components from "js/core.components";

@Module({
    components: components,
    controllers: controllers
})
export class HomeModule {
    static onAfterLoad() {

        let $collapse = $("#bs-example-navbar-collapse-1").find("li");

        $collapse.click(function (event) {
            $collapse.removeClass("active");
            $(event.target).parents("li").addClass("active");
        });

        $(".navbar-nav").find("a").on("click", function (e) {
            $(".navbar-toggle").trigger("click");
        });
    }
}