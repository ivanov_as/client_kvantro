"use strict";

import {Controller, GET} from "js/fr";
import tmp from "tmp/auth.page.html!text";

@Controller()
export class AuthController {

    @GET({
        path: "/auth",
        title: "Авторизация",
        template: tmp,
        themes: ["/css/auth.page.css"]
    })
    auth(params) {

    }
}
