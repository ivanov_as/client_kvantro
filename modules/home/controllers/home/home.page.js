"use strict";

import {Controller, GET} from "js/fr";
import tmp from "tmp/home.page.html!text"

@Controller()
export class HomeController {
    @GET({
        path: "/",
        title: "Главная",
        template: tmp,
        themes: ["css/home.page.css"]
    })
    home(params, next) {
        let _self = this;
        _self.app.class("advanced-filter").on("click", (event) => {
            event.preventDefault();
            _self.app.ui("search_panel").toggle("fadeIn");
        });
    }
}
