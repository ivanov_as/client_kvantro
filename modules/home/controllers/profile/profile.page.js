"use strict";

import {Controller, GET} from "js/fr";
import tmp from "tmp/profile.page.html!text";

@Controller()
export class ProfileController {

    @GET({
        path: "/profile",
        title: "Профиль",
        template: tmp,
        themes: ["css/profile.page.css"]
    })
    profile(params) {
    }
}
