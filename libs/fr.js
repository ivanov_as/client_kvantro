"use strict";

Ractive.DEBUG = false;

export function Component() {

    const properties = arguments[0];

    return function (target) {
        // Тег при добавлении на страницу
        target.selector = properties.selector;
        // Шаблон компонента
        target.template = properties.template || "";
        // Стилевое описание компонента
        target.theme = properties.theme || "";

        target._properties = {
            template: target.template,
            append: true,
            css: target.theme,
            components: _getComponents.call(properties),
            data: function () {
                return {}
            }
        };
        /** 1 шаг Подключаем основные свойства */
        target.instance = Ractive.extend(target._properties);
        /** 2 шаг Инициализируем сервисы */
        let _services = properties.services || [];
        for (let idx = 0; idx < _services.length; idx++) {
            target.instance = target.instance.extend(_services[idx].instance);
        }
        target.instance = target.instance.extend(_getListMethods.call(target));
    }
}

export function Service() {
    return function (target) {
        target.instance = _getListMethods.call(target);
    }
}

export function Controller() {
    return (target) => {
    }
}

export function Module() {
    const properties = arguments[0];

    return (target) => {
        target._name = target.name;

        if (Array.isArray(properties.components)) {
            target.prototype._controllers = properties.controllers;
        } else {
            target.prototype._controllers = $.map(properties.controllers, (value) => {
                return [value];
            });
        }

        if (Array.isArray(properties.components)) {
            target.prototype._components = properties.components;
        } else {
            target.prototype._components = $.map(properties.components, (value) => {
                return [value];
            });
        }
    }
}

export function AppModule() {

    const properties = arguments[0];

    let _modules = {};
    /** Инициализация маршрутов */
    page("*", function () {
        console.log("404 not found")
    });
    page();
    window.history.back();
    /** Конец инициализации машрутов */

    return (target) => {

        let lngModules = properties.modules.length;
        let lngComponents;
        let component;
        let module;
        let isBuild;

        let _components = {};

        /** Инициализация модулей */
        for (let idx = 0; idx < lngModules; idx++) {

            module = properties.modules[idx];
            _modules[module._name] = new module();

            lngComponents = _modules[module._name]._components.length;

            /** Инициализация компонентов */
            for (let idxComp = 0; idxComp < lngComponents; idxComp++) {
                component = _modules[module._name]._components[idxComp];
                _components[component.selector] = component.instance;
            }

            if (!isBuild) {
                target.prototype.instance = new Ractive({
                    el: "root",
                    template: "",
                    components: _components,
                    ui: _ui,
                    id: _id,
                    class: _class,
                    getComponents: function () {
                        return this.get("_components");
                    },
                    data: function () {
                        return {
                            _components: []
                        }
                    }
                });

                isBuild = true;
            }

            /** Инициализация контроллеров */
            _modules[module._name]._controllers.map((controller) => {
                controller.prototype.constructor.prototype.app = target.prototype.instance;
                return new controller;
            })
        }

        $(document).ready(function () {
            for (let idx = 0; idx < lngModules; idx++) {
                properties.modules[idx].onAfterLoad();
            }
        })
    }
}

export function GET() {

    const _properties = arguments[0];

    return function (target, name, descriptor) {
        page(_properties.path, function (params, next) {
            /** Устанавливаем название страницы */
            document.title = _properties.title || "";

            window.history.pushState(null, null, _properties.path);

            if (target.app) {
                let _styles = "";
                if (Array.isArray(_properties.themes)) {
                    for (let idx = 0; idx < _properties.themes.length; idx++) {
                        _styles += `<link rel="stylesheet" href="${_properties.themes[idx]}"/>`;
                    }
                }
                target.app.resetTemplate(_styles + (_properties.template || ""));
                // Инициализируем компоненты доступные на странице
                target.app.set("_components", target.app.findAllComponents());

                _activateDataSources.call(target.app);

                descriptor.value.call(target, params, next);
            }
        });
    }
}

/** Метод активирующий компоненты с dataSource */
function _activateDataSources() {
    let _dataSource;
    let _targetSource = this.getComponents().filter((component) => {
        return component.get("dataSource");
    });

    let lngTS = _targetSource.length;
    for (let idx = 0; idx < lngTS; idx++) {
        _dataSource = _targetSource[idx].getDataSource().getRecords();
        let lng = _dataSource.length;
        for (let i = 0; i < lng; i++) {
            this.ui(_dataSource[i].src).observeDataSet((newDataSet) => {
                if (newDataSet) {
                    _targetSource[idx].setRequestParams({
                        [_dataSource[i].foreignKey]: newDataSet.getCurrentRecord()["id"]
                    });
                    if (_targetSource[idx].refresh) _targetSource[idx].refresh();
                    // console.debug(_targetSource[idx].getRequestParams())
                }
            });
        }
    }
}

function _getListMethods() {

    let methods = Object.getOwnPropertyNames(this.prototype.constructor);
    let _methods = {};
    let lngMethods = methods.length;

    for (let idx = 0; idx < lngMethods; idx++) {
        if (methods[idx] !== "name" &&
            methods[idx] !== "length" &&
            methods[idx] !== "prototype" &&
            methods[idx] !== "instance" &&
            methods[idx] !== "_properties") {
            _methods[methods[idx]] = this.prototype.constructor[methods[idx]];
        }
    }
    return _methods;
}

function _getComponents() {

    this.components = this.components ? this.components : [];

    let lngComponents = this.components.length;
    let _components = {};

    for (let idx = 0; idx < lngComponents; idx++) {
        _components[this.components[idx].selector] = this.components[idx].instance;
    }

    return _components;
}

function _ui(name) {
    return this.get("_components").filter((component) => {
        return component.get("name") === name;
    })[0];
}

function _id(id) {
    return $(`#${id}`);
}

function _class(class_name) {
    return $(`.${class_name}`);
}