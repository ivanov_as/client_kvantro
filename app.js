"use strict";

import {AppModule} from '/js/fr';
import {HomeModule} from "/js/home.module";

@AppModule({
    modules: [HomeModule]
})
class Application {
    constructor() {
        console.log('Application initialize');
    }
}

// ИНИЦИАЛИЗАЦИЯ ПРИЛОЖЕНИЯ
new Application({});